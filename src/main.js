import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'

// 引入全局样式文件
import './assets/css/global.less';

// 引入字体文件
import './assets/font/iconfont.css';
import SocketService from './utils/socket'
// 对服务器进行webSocket的连接
SocketService.Instance.connect()
// 其他组件
Vue.prototype.$socket = SocketService.Instance

// axios 配置接口默认路径
axios.defaults.baseURL = 'http://127.0.0.1:9000/api/'
Vue.prototype.$http = axios;

// 把echarts挂载到Vue原型上 ，以便全局访问
Vue.prototype.$echarts = window.echarts


Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
