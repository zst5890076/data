# 数据实时监控

#### 介绍
本项目基于vue开发的数据实时监控系统,用vue搭配Echarts采用组件化方式，实现前后端交互。

#### 软件架构
 技术栈：
- Vue、
- VueX、
- Vue-router
- axios
- less
- WebSocket


#### 安装教程
    // 使用命令克隆项目
       git cloen https://gitee.com/zst5890076/data.git 
    // 使用命令 建立依赖
       npm install    
    // 使用命令  运行项目
       npm run serve    
